package com.example.movie_details.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.movie_details.model.Movie
import com.example.movie_details.model.MovieEntity
import com.example.movie_details.repository.MovieRepository
import kotlinx.coroutines.launch

class MovieViewModel(application: Application) : AndroidViewModel(application){
    private val movieRepository: MovieRepository

    // LiveData to hold the list of movies
    private val _movies: MutableLiveData<List<MovieEntity>> = MutableLiveData()

    // Expose LiveData to the UI
    fun getMovies(): LiveData<List<MovieEntity>> {
        return _movies
    }

    // Function to fetch movies from the repository
    fun fetchMovies() {
        movieRepository.getMovies { movies ->
            _movies.postValue(movies)
        }
    }
    // Function to update the favorite status of a movie
    fun toggleFavoriteStatus(movie: MovieEntity) {
        viewModelScope.launch {
            movie.isFavorite = !movie.isFavorite
            movieRepository.updateMovie(movie)
        }
    }

    init {
        movieRepository = MovieRepository(application)
    }
}