package com.example.movie_details.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "movies")
data class MovieEntity(
    @PrimaryKey val id: String,
    val trackName: String,
    val artistName: String,
    val primaryGenreName: String,
    val longDescription: String,
    val artworkUrl100: String,
    val trackHdPrice: Double,
    var isFavorite: Boolean = false
)
