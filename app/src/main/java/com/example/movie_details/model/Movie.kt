package com.example.movie_details.model

import android.os.Parcel
import android.os.Parcelable

data class Movie(
    val trackName: String,
    val artistName: String,
    val releaseDate: String,
    val primaryGenreName: String,
    val longDescription: String,
    val artworkUrl100: String,
    val trackHdPrice: Double,
    var isFavorite: Boolean = false
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readDouble(),
        parcel.readByte() != 0.toByte()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(trackName)
        parcel.writeString(artistName)
        parcel.writeString(releaseDate)
        parcel.writeString(primaryGenreName)
        parcel.writeString(longDescription)
        parcel.writeString(artworkUrl100)
        parcel.writeDouble(trackHdPrice)
        parcel.writeByte(if (isFavorite) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Movie> {
        override fun createFromParcel(parcel: Parcel): Movie {
            return Movie(parcel)
        }

        override fun newArray(size: Int): Array<Movie?> {
            return arrayOfNulls(size)
        }
    }
}
