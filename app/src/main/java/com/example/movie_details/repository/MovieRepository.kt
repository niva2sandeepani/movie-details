package com.example.movie_details.repository
import android.content.Context
import com.example.movie_details.model.Movie
import com.example.movie_details.model.MovieDao
import com.example.movie_details.model.MovieDatabase
import com.example.movie_details.model.MovieEntity
import com.example.movie_details.util.MovieService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MovieRepository(private val context: Context) {
    private val movieDao: MovieDao

    init {
        val database = MovieDatabase.getDatabase(context)
        movieDao = database.movieDao()
    }


    private val retrofit = Retrofit.Builder()
        .baseUrl("https://itunes.apple.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val service = retrofit.create(MovieService::class.java)

    fun getMovies(callback: (List<MovieEntity>) -> Unit) {
        service.getMovies().enqueue(object : Callback<MovieData> {
             override fun onResponse(call: Call<MovieData>, response: Response<MovieData>) {
                if (response.isSuccessful) {
                    val movieData = response.body()
                    val movies = movieData?.results?.map { result ->
                        MovieEntity(
                            result.trackName,
                            result.artistName,
                            result.releaseDate,
                            result.primaryGenreName,
                            result.longDescription,
                            result.artworkUrl100,
                            result.trackHdPrice
                        )
                    } ?: emptyList()
                    callback(movies)
                } else {
                    // Handle error
                    callback(emptyList())
                }
            }

            override fun onFailure(call: Call<MovieData>, t: Throwable) {
                // Handle failure
                callback(emptyList())
            }
        })
    }

    // Function to get movies from Room database
    fun getMoviesFromDatabase(): List<MovieEntity> {
        return movieDao.getAllMovies()
    }
    suspend fun insertMovie(movie: MovieEntity) {
        withContext(Dispatchers.IO) {
            movieDao.insert(movie)
        }
    }
    suspend fun updateMovie(movie: MovieEntity) {
        withContext(Dispatchers.IO) {
            movieDao.updateMovie(movie)
        }
    }

    // Function to update the favorite status of a movie in the database
    suspend fun updateFavoriteStatus(movie: MovieEntity) {
        movieDao.updateMovie(movie)
    }

    data class MovieData(
        val resultCount: Int,
        val results: List<Movie>
    )


}