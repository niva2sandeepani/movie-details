package com.example.movie_details.util

import com.example.movie_details.repository.MovieRepository
import retrofit2.Call
import retrofit2.http.GET

interface MovieService {
    @GET("search?term=star&country=au&media=movie&all")
    fun getMovies(): Call<MovieRepository.MovieData>
}