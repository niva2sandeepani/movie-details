package com.example.movie_details.ui

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.movie_details.R
import com.example.movie_details.model.Movie
import com.example.movie_details.model.MovieEntity
import com.example.movie_details.viewModel.MovieViewModel
import com.squareup.picasso.Picasso

class MovieAdapter(private var movies: List<MovieEntity>, private val viewModel: MovieViewModel) :
    RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false)
        return ViewHolder(view, this)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = movies[position]
        holder.bind(movie)
        val context = holder.itemView.context
        holder.itemView.setOnClickListener {
            // Open image in full-screen mode
            val intent = Intent(context, DetailActivity::class.java)
            //intent.putExtra("movie",movie)
            //context.startActivity(intent)

        }
    }

    override fun getItemCount(): Int = movies.size

    fun updateData(newMovies: List<MovieEntity>) {
        movies = newMovies
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View, private val adapter: MovieAdapter) : RecyclerView.ViewHolder(itemView) {
        private val movieNameTextView: TextView = itemView.findViewById(R.id.movieNameTextView)
        private val artistNameTextView: TextView = itemView.findViewById(R.id.artistNameTextView)
        private val genreTextView: TextView = itemView.findViewById(R.id.genreTextView)
        private val PriceTextView: TextView = itemView.findViewById(R.id.PriceTextView)
        private val movieImageView: ImageView = itemView.findViewById(R.id.movieImageView)
        val imgCheckbox = itemView.findViewById<ImageView>(R.id.checkboxOFF)

        fun bind(movie: MovieEntity) {
            itemView.apply {
                movieNameTextView.text = movie.trackName
                artistNameTextView.text = movie.artistName
                genreTextView.text = movie.primaryGenreName
                PriceTextView.text = "$" + movie.trackHdPrice.toString()

                var isFavourite = movie.isFavorite

                imgCheckbox.setImageResource(if (movie.isFavorite) R.drawable.ic_checked_checkbox else R.drawable.ic_empty_checkbox)

                imgCheckbox.setOnClickListener {
                    adapter.viewModel.toggleFavoriteStatus(movie)
                    imgCheckbox.setImageResource(if (movie.isFavorite) R.drawable.ic_checked_checkbox else R.drawable.ic_empty_checkbox)
                }
                Picasso.get().load(movie.artworkUrl100).into(movieImageView)
            }
        }
    }
}
