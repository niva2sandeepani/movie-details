package com.example.movie_details.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.example.movie_details.R
import com.example.movie_details.model.Movie
import com.squareup.picasso.Picasso

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        // Retrieve movie details from intent extras
        val movie = intent.getParcelableExtra<Movie>("movie")

        // Update UI with movie details
        findViewById<TextView>(R.id.movieTitleTextView).text = movie?.trackName
        findViewById<TextView>(R.id.movieDescriptionTextView).text = movie?.longDescription
        Picasso.get().load(movie?.artworkUrl100).into(findViewById<ImageView>(R.id.movieImageView))

        // Handle favorite button click
        val favoriteButton: Button = findViewById(R.id.favoriteButton)
        favoriteButton.setOnClickListener {
            // Toggle favorite status of the movie and update UI accordingly
            movie?.isFavorite = !movie?.isFavorite!!
            // Update favorite button text based on favorite status
            favoriteButton.text = if (movie?.isFavorite == true) "Remove Favorite" else "Add Favorite"
        }

        // Handle back button click
        val backButton: ImageButton = findViewById(R.id.backButton)
        backButton.setOnClickListener {
            finish() // Finish the activity when back button is clicked
        }
    }
}