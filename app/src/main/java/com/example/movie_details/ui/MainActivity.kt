package com.example.movie_details.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movie_details.R
import com.example.movie_details.model.MovieDatabase
import com.example.movie_details.viewModel.MovieViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var movieAdapter: MovieAdapter
    private val movieViewModel: MovieViewModel by viewModels()
    private lateinit var listRecycleView: RecyclerView
    private lateinit var searchView: SearchView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        listRecycleView  = findViewById(R.id.listRecycleView)
        movieAdapter = MovieAdapter(emptyList(),movieViewModel)
        listRecycleView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = movieAdapter
        }

        // Observe changes in movie data from the ViewModel
        movieViewModel.getMovies().observe(this, Observer { movies ->
            movieAdapter.updateData(movies)
        })

        val movieDao = MovieDatabase.getDatabase(applicationContext).movieDao()

//        // Set up search functionality
//        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
//            override fun onQueryTextSubmit(query: String?): Boolean {
//                return false
//            }
//
//            override fun onQueryTextChange(newText: String?): Boolean {
//              //  movieAdapter.filter.filter(newText)
//                return true
//            }
//        })
        // Fetch movies from the ViewModel
        movieViewModel.fetchMovies()
    }
}